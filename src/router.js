import Vue from 'vue'
import Router from 'vue-router'
// Páginas
import Calendario from './pages/Calendario'

Vue.use(Router);

export default new Router({
  // mode: 'history',
  routes: [
    // Páginas
    {path: '/',  component: Calendario}
  ]
})
