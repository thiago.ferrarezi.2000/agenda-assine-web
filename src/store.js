import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
    checkErros: false
};

const mutations = {
  setCheckErros (state, payload) {
    state.checkErros = payload
  },
};
const getters = {
  getCheckErros (state) {
    return state.checkErros
  },
};

export default new Vuex.Store({state, mutations, getters})
