import axios from "axios";

const api = axios.create({
    baseURL: "http://localhost:8021/tarefa",
});

export default api
